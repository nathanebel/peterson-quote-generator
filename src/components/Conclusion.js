import React from 'react'

const Conclusion = props => {
  return <span className="conclusion six">{props.text}</span>
}

export default Conclusion
