import React from 'react'

const SpicyVerb = props => {
  return <span className='spicyVerb two'>{props.text}</span>
}

export default SpicyVerb
