import React from 'react'

const EvilWeapons = props => {
  return <span className='evilWeapons five'>{props.text}</span>
}

export default EvilWeapons
