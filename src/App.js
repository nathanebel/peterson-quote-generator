import React, { Component } from 'react'
import Conclusion from './components/Conclusion.js'
import EvilWeapons from './components/EvilWeapons.js'
import FavouriteThings from './components/FavouriteThings.js'
import SpicyVerb from './components/SpicyVerb.js'
import Villains from './components/Villains.js'
import './App.css'
const arrayMove = require('array-move');

const becauseOfTheir = 'because of their '

class App extends Component {
  constructor () {
    super()
    this.state = {
      villains: [
        'Postmodern Neomarxists ',
        'Feminists (who secretly crave domination) ',
        'Leftist academics ',
        'Dangerous idealogues ',
        'Derrida and Foucalt ',
        'Indoctrinated students ',
        'Social justice types ',
        'Radical trans activists ',
        'Politically correct HR departments ',
        'Actual communists ',
        'The Left ',
        'Millennials with a victimhood mentality '
      ],

      spicyVerb: [
        'are totally corrupting ',
        'have zero respect for ',
        'want to annihilate ',
        "don't bloody believe in ",
        'will quickly infect ',
        'unleash the Chaos Dragon of ',
        'dismiss and transgress ',
        'must be stopped from attacking ',
        'will make gulags out of ',
        'feminize and weaken '
      ],

      favouriteThings: [
        'the dominance hierarchy ',
        'the metaphorical substrate ',
        'Western values ',
        'the classical humanities ',
        'the individual ',
        "the Hero's Journey ",
        'the fabric of being ',
        "Solzhenitsyn's genius ",
        "Carl Jung's legacy ",
        'IQ testing ability to determine achievement ',
        'the divine Logos ',
        'the inescapable tragedy and suffering of life ',
        "the humble lobster's quest "
      ],

      evilWeapons: [
        'murderous equity doctrine ',
        'dangerous egalitarian utopia ',
        'hatred of Objective Truth ',
        'compelled speech ',
        'group identity ',
        'propaganda from Frozen ',
        'radical collectivism ',
        'lens of power for everything ',
        'disdain for Being ',
        'ideological bill C-16 ',
        'low serotonin levels and poor posture ',
        "totalitarian ideology which I've studied for decades ",
        'unclean rooms '
      ],

      conclusion: [
        "and we can't even have a conversation about it!",
        'so just ask the Kulaks how that worked out!',
        'and no one is talking about it!',
        'as you can bloody well imagine!',
        'just like Nietzsche prophesized!',
        'so you should sign up for the Self Authoring Suite.',
        '[while still ignoring the original question] so let me ask you this...',
        'and you can be damn sure about that!'
      ]
    }
  }
  

  pickRandom = array => {
    return array[Math.floor(Math.random() * array.length)]
  }

  updateWords = () => {
    this.setState({ 
      villainsKey : Math.random(),
      spicyKey : Math.random(),
      favouriteKey : Math.random(),
      evilKey : Math.random(),
      conclusionKey : Math.random(),
    });
  }

  render () {
    return (
      <div className="appContainer">

        <div className="nonsense">
          <Villains key={this.state.villainsKey} text={this.pickRandom(this.state.villains)} />
          <SpicyVerb key={this.state.spicyKey} text={this.pickRandom(this.state.spicyVerb)} />
          <FavouriteThings key={this.state.favouriteKey} text={this.pickRandom(this.state.favouriteThings)} />
          <EvilWeapons key={this.state.evilKey} text={this.pickRandom(this.state.evilWeapons)} />
          <Conclusion key={this.state.conclusionKey} text={this.pickRandom(this.state.conclusion)} />
        </div>

        <button onClick={this.updateWords}>
          click
        </button>
      </div>
    )
  }
}

export default App
